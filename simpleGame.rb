class Player
	attr_accessor :name, :health, :power, :resistance
	def initialize(n, h, pow, r)
		@name = n
		@health = h
		@power = pow
		@resistance	= r
	end
	def isAlive
		@health>0
	end
	def hit(opponent)
		critical = rand(200)
		if critical>150
			procent = rand(200)
		else
			procent = rand(100)
		end
		impact	= (self.power*procent)/100
		if impact>self.power
			puts "#{name} impact #{impact} critical hit"
		else
			puts "#{name} impact #{impact}"
		end
		if impact>opponent.resistance
			opponent.health -= (impact-opponent.resistance)
		end
	end
	def to_s
		"#{name}: Health: #{health}, Power: #{power}, Resistance: #{resistance}"
	end
end

def figth(p1, p2)
	while p1.isAlive && p2.isAlive
		p1.hit(p2)
		p2.hit(p1)
		show_info(p1, p2)
	end
	
	if p1.isAlive
		puts "#{p1.name} WON!"
	elsif p2.isAlive
		puts "#{p2.name} WON!"
	else
		puts "TIE!"
	end
end

def show_info(*p)
	p.each {|x| puts x}
end

puts "Players info"
p1 = Player.new("Player 1", 1+rand(100), 1+rand(20), 1+rand(20))
p2 = Player.new("Player 2", 1+rand(100), 1+rand(20), 1+rand(20))

show_info(p1, p2)

puts "LET's FiGHT!"
figth(p1, p2)
